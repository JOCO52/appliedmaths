﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
using System;

namespace AppliedMathsAssessment
{
    class Player : ModelObject
    {
        // ------------------
        // Data
        // ------------------
        private const float TURN_SPEED = 1f;
        private const float MOVE_ACCEL = 1000f;
        private const float SHOOT_SPEED = 200f;
        private Bullet bulletPrototype;
        private Game1 game;
        private float firingDistance;
        private float firingHeight;
        private const float FIRE_DISTANCE = 20f;
        private const float FIRE_COOLDOWN = 0.5f;
        private float timeSinceLastFire = 0;
        private ModelObject[] pips = new ModelObject[10];
        private SoundEffect wallHit;
        private SoundEffect cannonFire;
        private SoundEffectInstance tankMove;
        private const float MAX_SPEED_FOR_SFX_CALCS = 100f;

        // ------------------
        // Behaviour
        // ------------------
        public void Initialise(Bullet newBullet, ModelObject pipPrototype, Game1 newGame, SoundEffect newWallHit, SoundEffect newCannonFire, SoundEffect newTankMove)
        {
            firingDistance = 35f;
            firingHeight = 15f;
            isTrigger = false;

            bulletPrototype = newBullet;
            game = newGame;
            wallHit = newWallHit;
            cannonFire = newCannonFire;
            tankMove = newTankMove.CreateInstance();
            tankMove.Volume = 0;

            // Setup pips
            for (int i = 0; i < pips.Length; ++i)
            {
                pips[i] = new ModelObject();
                pipPrototype.CopyTo(pips[i]);
                pips[i].SetSclae(new Vector3(0.01f));
                game.AddObject(pips[i]);
            }
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Calculate sfx sound
            float currentSpeed = velocity.Length();
            float speedFraction = currentSpeed / MAX_SPEED_FOR_SFX_CALCS;
            if (speedFraction > 1) speedFraction = 1;
            tankMove.Volume = speedFraction;
            if (tankMove.State != SoundState.Playing)
                tankMove.Play();

            // Set our acceleration this frame to 0 by default
            acceleration = new Vector3(0, 0, 0);

            // Rotate 
            if (Keyboard.GetState().IsKeyDown(Keys.Left)) rotation.Y += TURN_SPEED * dt;
            if (Keyboard.GetState().IsKeyDown(Keys.Right)) rotation.Y -= TURN_SPEED * dt;

            // Accelerate
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                acceleration.X = (float)Math.Sin(rotation.Y) * MOVE_ACCEL; // Don't need to use dt as we're setting, not adding
                acceleration.Z = (float)Math.Cos(rotation.Y) * MOVE_ACCEL; // Don't need to use dt as we're setting, not adding
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                acceleration.X = -(float)Math.Sin(rotation.Y) * MOVE_ACCEL * 0.5f; // Don't need to use dt as we're setting, not adding
                acceleration.Z = -(float)Math.Cos(rotation.Y) * MOVE_ACCEL * 0.5f; // Don't need to use dt as we're setting, not adding
            }

            // Shoot bullet

            // Calculate bullet trajectory (used for pips and for actual shooting)
            Vector3 bulletDirection = Vector3.Zero;
            bulletDirection.X = (float)Math.Sin(rotation.Y);
            bulletDirection.Z = (float)Math.Cos(rotation.Y);
            bulletDirection.Y = 0.2f;
            bulletDirection.Normalize();
            Vector3 bulletVelocity = bulletDirection * SHOOT_SPEED;
            Vector3 currentFiringPoint = position + firingDistance * bulletDirection;
            currentFiringPoint.Y = firingHeight;
            
            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 5 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            // OLD VERSION, DEPRECATED
            // Re-write this to position pips along projectile path curve
            for (int i = 0; i < pips.Length; ++i)
            {
                pips[i].SetPosition(position);
            }

            ///////////////////////////////////////////////////////////////////
            // END TASK 5 CODE
            ///////////////////////////////////////////////////////////////////

            // Actually shoot
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && timeSinceLastFire >= FIRE_COOLDOWN)
            {
                cannonFire.Play();
                Bullet newBullet = new Bullet();
                bulletPrototype.CopyTo(newBullet);
                newBullet.Fire(currentFiringPoint, bulletVelocity);
                game.AddObject(newBullet);
                timeSinceLastFire = 0;
            }
            timeSinceLastFire += dt;

            base.Update(gameTime);
        }

        // ------------------
        public override void HandleCollision(PhysicsObject other)
        {
            if (other is Enemy)
            {
                game.LoseGame();
            }
            else if (other is BasicCuboid)
            {
                wallHit.Play();
            }
            base.HandleCollision(other);
        }
        // ------------------

    }
}
